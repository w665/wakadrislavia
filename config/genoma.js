const fs = require("fs");
const width = 730;
const height = 1080;
const dir = __dirname;
const description = "Wakadrislavia cuenta con una estructura ADN generada con Node.js";
const baseImageUri = "https://wakadrislavia.xyz";
const startEditionFrom = 1;
const endEditionAt = 1296;
const editionSize = 1296;
const raceWeights = [
  {
    value: "king",
    from: 1,
    to: 1296,
  }
];

const races = {
  king: {
    name: "King",
    layers: [
      {
        name: "Body",
        elements: [
          {
            id: 'B1',
            name: "Body 1",
            path: `${dir}/input/03-body/1.png`,
            weight: 100,
          },
          {
            id: 'B2',
            name: "Body 2",
            path: `${dir}/input/03-body/1.png`,
            weight: 100,
          },
          {
            id: 'B3',
            name: "Body 3",
            path: `${dir}/input/03-body/1.png`,
            weight: 100,
          },
          {
            id: 'B4',
            name: "Body 4",
            path: `${dir}/input/03-body/1.png`,
            weight: 100,
          },
          {
            id: 'B5',
            name: "Body 5",
            path: `${dir}/input/03-body/1.png`,
            weight: 100,
          },
          {
            id: 'B6',
            name: "Body 6",
            path: `${dir}/input/03-body/1.png`,
            weight: 100,
          },
        ],
        order: 2,
        structure: true,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Head",
        elements: [
          {
            id: 'H1',
            name: "Head 1",
            path: `${dir}/input/04-head/1.png`,
            weight: 100,
          },
          {
            id: 'H2',
            name: "Head 2",
            path: `${dir}/input/04-head/2.png`,
            weight: 100,
          },
          {
            id: 'H3',
            name: "Head 3",
            path: `${dir}/input/04-head/3.png`,
            weight: 100,
          },
          {
            id: 'H4',
            name: "Head 4",
            path: `${dir}/input/04-head/4.png`,
            weight: 100,
          },
          {
            id: 'H5',
            name: "Head 5",
            path: `${dir}/input/04-head/5.png`,
            weight: 100,
          },
          {
            id: 'H6',
            name: "Head 6",
            path: `${dir}/input/04-head/6.png`,
            weight: 100,
          },
          
        ],
        order: 3,
        structure: true,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Weapon",
        elements: [
          {
            id: 'W1',
            name: "Weapon 1",
            path: `${dir}/input/05-weapon/1.png`,
            weight: 100,
          },
          {
            id: 'W2',
            name: "Weapon 2",
            path: `${dir}/input/05-weapon/2.png`,
            weight: 100,
          },
          {
            id: 'W3',
            name: "Weapon 3",
            path: `${dir}/input/05-weapon/3.png`,
            weight: 100,
          },
          {
            id: 'W4',
            name: "Weapon 4",
            path: `${dir}/input/05-weapon/4.png`,
            weight: 100,
          },
          {
            id: 'W5',
            name: "Weapon 5",
            path: `${dir}/input/05-weapon/5.png`,
            weight: 100,
          },
          {
            id: 'W6',
            name: "Weapon 6",
            path: `${dir}/input/05-weapon/6.png`,
            weight: 100,
          },
        ],
        order: 4,
        structure: true,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Crown",
        elements: [
          {
            id: 'C1',
            name: "Crown 1",
            path: `${dir}/input/06-crown/1.png`,
            weight: 100,
          },
          {
            id: 'C2',
            name: "Crown 2",
            path: `${dir}/input/06-crown/2.png`,
            weight: 100,
          },
          {
            id: 'C3',
            name: "Crown 3",
            path: `${dir}/input/06-crown/3.png`,
            weight: 100,
          },
          {
            id: 'C4',
            name: "Crown 4",
            path: `${dir}/input/06-crown/4.png`,
            weight: 100,
          },
          {
            id: 'C5',
            name: "Crown 5",
            path: `${dir}/input/06-crown/5.png`,
            weight: 100,
          },
          {
            id: 'C6',
            name: "Crown 6",
            path: `${dir}/input/06-crown/6.png`,
            weight: 100,
          },
          
        ],
        order: 5,
        structure: true,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Deck",
        elements: [
          {
            id: '-000-',
            name: "Deck",
            path: `${dir}/input/deck/0.png`,
            weight: 100,
          },
        ],
        order: 10,
        structure: false,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Suit",
        elements: [
          {
            id: 'H',
            name: "Hearts",
            path: `${dir}/input/02-frame/1.png`,
            weight: 100,
          },
          {
            id: 'C',
            name: "Clovers",
            path: `${dir}/input/02-frame/2.png`,
            weight: 100,
          },
          {
            id: 'P',
            name: "Pikes",
            path: `${dir}/input/02-frame/3.png`,
            weight: 100,
          },
          {
            id: 'T',
            name: "Tiles",
            path: `${dir}/input/02-frame/4.png`,
            weight: 100,
          },
        ],
        order: 1,
        structure: true,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Mode",
        elements: [
          {
            id: 'X',
            name: "Computer",
            path: `${dir}/input/mode/0.png`,
            weight: 100,
          },
          {
            id: 'Y',
            name: "Human",
            path: `${dir}/input/mode/0.png`,
            weight: 100,
          },
          {
            id: 'Z',
            name: "Special Guest",
            path: `${dir}/input/mode/0.png`,
            weight: 100,
          },
        ],
        order: 11,
        structure: false,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      },
      {
        name: "Rarity",
        elements: [
          {
            id: 'N',
            name: "No Rarity",
            path: `${dir}/input/rarity/0.png`,
            weight: 100,
          },
          {
            id: "WW",
            name: "Wakararity",
            path: `${dir}/input/rarity/W1.png`,
            weight: 100,
          },
        ],
        order: 12,
        structure: false,
        position: { x: 0, y: 0 },
        size: { width: width, height: height },
      }
    ],
  },
};

module.exports = {
  width,
  height,
  description,
  baseImageUri,
  editionSize,
  startEditionFrom,
  endEditionAt,
  races,
  raceWeights,
};
