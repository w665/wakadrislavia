const fs = require("fs");
const path = require('path');
var genoma;
var variations = 6; //objetos o variaciones de cada gen
var genoma_size = 4; // genes de estructura
var bigDNAList = [];
var dnaList = [];
var metadataList = [];
var attributesList = [];
var max_collection = Math.pow(variations, genoma_size); // debe ser calculado 
const controller = {};
const {
    width, 
    height,
    description,
    baseImageUri,
    editionSize,
    startEditionFrom,
    endEditionAt,
    races,
    raceWeights,
} = require("../../config/genoma.js");
const console = require("console");
const { createCanvas, loadImage } = require("canvas");
const canvas = createCanvas(width, height);
const ctx = canvas.getContext("2d");



controller.index = async (req, res, next) => {
    res.render('index', { 
        title: 'Hola DNAList',  
        DNAList: await getDNAList(),
    });
}

const getDNAList = async () => {
    genoma = initGenoma();
    let editionCount = startEditionFrom;
    while (editionCount <= endEditionAt) { 
      let race = getRace(editionCount);
      let newDna = createDna(races, race, editionCount);
      if (isDnaUnique(dnaList, newDna)) {
        dnaList.push(newDna);
        editionCount++;
      }
    }
    let first_gens = dnaList;

    //Sort
    first_gens = waka_sort(first_gens);

    //Mazo
    let gen_4 = deckSequencer(max_collection);
    //Palo
    let gen_5 = genSequencer(races.king.layers[5],1,max_collection);
    //Dibujado
    let gen_6 = genDrawed();
    let gen_7 = genRarity();


    first_gens.forEach((gen,key) => {
        let this_gen4 = gen_4[key];
        let this_gen5 = gen_5[key];
        let this_gen6 = gen_6[key];
        let this_gen7 = gen_7[key];
        gen.push(this_gen4,this_gen5,this_gen6,this_gen7);
        bigDNAList.push(gen);
    });

    
    let limit_debug = 200;
    for (let index = 0; index < limit_debug; index++) {
        createKing(bigDNAList[index],index);
    }
    return bigDNAList;
};

const initGenoma = () => { 
    let max_potencias = [];
    for (let index = genoma_size-1; index > -1; index--) {
        max_potencias.push(Math.pow(variations,index));
    } 
    //Genes de estructura
    let gen_0 = genSequencer(races.king.layers[0],max_potencias[0],max_collection);
    let gen_1 = genSequencer(races.king.layers[1],max_potencias[1],max_collection);
    let gen_2 = genSequencer(races.king.layers[2],max_potencias[2],max_collection);
    let gen_3 = genSequencer(races.king.layers[3],max_potencias[3],max_collection);

    let genoma = [gen_0,gen_1,gen_2,gen_3];
    return genoma;
};
  
const isDnaUnique = (_DnaList = [], _dna = []) => {
    let foundDna = _DnaList.find((i) => i.join("") === _dna.join(""));
    return foundDna == undefined ? true : false;
};

const getRace = (_editionCount) => {
    let race = "king";
    raceWeights.forEach((raceWeight) => {
        if (_editionCount >= raceWeight.from && _editionCount <= raceWeight.to) {
        race = raceWeight.value;
        }
    });
    return race;
};

const createDna = (_races, _race, editionCount) => {
    let DNA = [];
    genoma.forEach((gen) => {
        let num = 0;
        num = gen[(editionCount-1)];
        DNA.push(num);
    });
    return DNA;
};

const genSequencer = (layers,max_potencia,max_collection) => {
    let the_gen = [];
    let potencia = 0;
    let the_index = 0;
    for (let index = 0; index < max_collection; index++) {
        let the_id = layers.elements[the_index].id;
        the_gen.push(the_id);
        if(potencia>=(max_potencia-1)){
            the_index++;
            potencia=0;
        }else{
            potencia++;
        }
        if(the_index==(layers.elements.length)){the_index=0;}
    }
    return the_gen;
};

const deckSequencer = (max_collection) => {
    let the_gen = [];
    let potencia = 1;
    let the_index = 1;
    for (let index = 0; index < max_collection; index++) {
        the_gen.push(the_index);

        if(potencia==4){
            the_index++;
            potencia=1;
        }else{
            potencia++;
        }
    }
    return the_gen;
};

// Gen que indica si es creado por Waka, CPU o Invitados
const genDrawed = () => {
    let genDrawed = [];
    for (let index = 0; index < 18; index++) {
        genDrawed = genDrawed.concat(drawedBatch(index+1));
    }
    return genDrawed;
};
const drawedBatch = () => {
    let drawedBatch = [];
    let value; 
    for (let index = 0; index < 72; index++) {
        value = (index>63)?"Y":"X";
        drawedBatch.push(value);
    }
    drawedBatch = waka_sort(drawedBatch);
    return drawedBatch;
};

// Gen de Rareza
const genRarity = () => {
    let genRarity = [];
    let value; 
    let rarity = 24;//cantidad por max_collection
    for (let index = 0; index < max_collection; index++) {
        value = (index>((max_collection-1)-rarity))?"Ñ":"";
        genRarity.push(value);
    }
    genRarity = waka_sort(genRarity);
    return genRarity;
};

//Waka Sort
const waka_sort = (the_array) => {
    the_array.sort(() => Math.random() - 0.5);
    return the_array;
};

/// IMAGE CONTROLLER

async function createKing(newDna,_pos) {
    
let race = 'king';
let results = constructLayerToDna(newDna, races, race);
      let loadedElements = []; //promise array

      //Fondo por defecto para todas las imagenes
      let this_path = path.join(__dirname,`../../config/input/01-bg/1.png`);
        loadedElements.push(loadLayerImg({
            name: 'background',
            position: { x: 0, y: 0 },
            size: { width: width, height: height },
            order: 0,
            selectedElement: {path: `${this_path}`},
        }));
    
        //Recorrido de capas 
      results.forEach((layer) => {
        loadedElements.push(loadLayerImg(layer));
      });
      console.log(results);

      await Promise.all(loadedElements).then((elementArray) => {
        _position = _pos+1;
        ctx.clearRect(0, 0, width, height);
        // drawBackground(); //crea fondo de color random
        elementArray.forEach((element) => {
          drawElement(element); //recorre capas, arma el buffer y genera atributelist por cada gen
        });
        signImage(`#${newDna}`);//le agrega un tag 
        saveImage(_position);//guarda el archivo
        addMetadata(newDna, max_collection);//guarda el meta generado en json individual
        saveMetaDataSingleFile(max_collection);//guarda el meta grupal
        console.log(
          `Created edition: ${max_collection}, Race: ${race} with DNA: ${newDna}`
        );
      });
}

const constructLayerToDna = (_dna = [], _races = [], _race) => {

    let all_layers = _races[_race].layers;

    let mappedDnaToLayers = all_layers.map((layer, index) => {
        if(layer.structure){
            let selectedElement = layer.elements.find((e) => e.id == _dna[index]);
            return {
                name: layer.name,
                position: layer.position,
                size: layer.size,
                order: layer.order,
                selectedElement: selectedElement,
            };
        }
        return false;
    });
    mappedDnaToLayers = mappedDnaToLayers.sort((a, b) => { return a.order > b.order ? 1 : -1});
    return mappedDnaToLayers.filter((layer)=>{ return (layer) ? true:false;});
};
const loadLayerImg = async (_layer) => {
    return new Promise(async (resolve) => {
      const image = await loadImage(`${_layer.selectedElement.path}`);
      resolve({ layer: _layer, loadedImage: image }); 
    });
  };

const drawElement = (_element) => {
ctx.drawImage(
    _element.loadedImage,
    _element.layer.position.x,
    _element.layer.position.y,
    _element.layer.size.width,
    _element.layer.size.height
);
addAttributes(_element);
};

const addMetadata = (_dna, _edition) => {
let dateTime = Date.now();
let tempMetadata = {
    dna: _dna.join(""),
    name: `#${_edition}`,
    description: description,
    image: `${baseImageUri}/${_edition}.png`,
    edition: _edition,
    date: dateTime,
    attributes: attributesList,
};
metadataList.push(tempMetadata);
attributesList = [];
};

const addAttributes = (_element) => {
    let selectedElement = _element.layer.selectedElement;
    attributesList.push({
        trait_type: _element.layer.name,
        value: selectedElement.name,
    });
};

const saveImage = (_editionCount) => {
    let this_path = path.join(__dirname,`../../public/output/${_editionCount}.png`)
    fs.writeFileSync(
      `${this_path}`,
      canvas.toBuffer("image/png")
    );
  };
  
const signImage = (_sig) => {
    ctx.fillStyle = "#ffffff";
    ctx.font = "bold 30pt Verdana";
    ctx.textBaseline = "top";
    ctx.textAlign = "left";
    ctx.fillText(_sig, 40, 40);
};

const writeMetaData = (_data) => {
    let this_path = path.join(__dirname,`../../public/output/_metadata.json`)
    fs.writeFileSync(`${this_path}`, _data);
  };

const saveMetaDataSingleFile = (_editionCount) => {
    let this_path = path.join(__dirname,`../../public/output/${_editionCount}.json`)
    fs.writeFileSync(
        `${this_path}`,
        JSON.stringify(metadataList.find((meta) => meta.edition == _editionCount))
    );
};



/// FIN IMAGE CONTROLLER









module.exports = controller;