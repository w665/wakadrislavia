// Import and Require
var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var hbs = require('hbs');
var path = require('path');
var app = express();

// view engine setup
hbs.registerPartials(path.join(__dirname,'src/views/partials'), (err)=>{});
app.set('views', path.join(__dirname, 'src/views'));
app.set('view engine', 'hbs');

// Set App
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Import Routes
var indexRouter = require('./src/routes/index.routes');
var usersRouter = require('./src/routes/users.routes');

// Routes
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use(function(req, res, next) { next(createError(404)); });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


hbs.registerHelper("inc", function(value, options)
{
    return parseInt(value) + 1;
});

module.exports = app;
